import Layout from "../components/layout";

export default function Page() {
  return (
    <Layout>
      <h1>PTTPK OAuth 2</h1>
      <p>
        This is an example site to demonstrate how to use{" "}
        <a href={`https://next-auth.js.org`}>NextAuth.js</a> for authentication.
      </p>
    </Layout>
  );
}
